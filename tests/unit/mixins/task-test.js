import Ember from 'ember';
import TaskMixin from 'alerttool/mixins/task';
import { module, test } from 'qunit';

module('Unit | Mixin | task');

// Replace this with your real tests.
test('it works', function(assert) {
  let TaskObject = Ember.Object.extend(TaskMixin);
  let subject = TaskObject.create();
  assert.ok(subject);
});
