export function initialize() {
  // application.inject('route', 'foo', 'service:foo');
  moment.tz.setDefault("Asia/Calcutta");
}

export default {
  name: 'application',
  initialize
};
