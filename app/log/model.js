import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';

export default Model.extend({
    jira:attr("string"),
    description:attr("string"),
    status:attr("string"),
    by:attr("string"),
    time:attr("number"),
    task:belongsTo('task')
});
