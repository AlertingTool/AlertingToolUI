import Ember from 'ember';

export default Ember.Route.extend({
    beforeModel: function() {
        var r=this.get('session').fetch().catch(()=>{
                this.transitionTo("/");
        });
        return r;
    },
    actions: {
        signIn: function(email,password) {
            this.get('session').open('firebase', {
                provider: 'password',
                email: email,
                password: password
            }).then(function(data) {
                console.log(data.currentUser);
            }).catch((error)=>{
                this.controller.set("error",error);
            });
        },
        signOut: function() {
            this.get('session').close();
        }
    }
});
