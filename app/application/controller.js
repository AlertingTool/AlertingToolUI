import Ember from 'ember';

export default Ember.Controller.extend({
    clock: Ember.inject.service('clock-service'),
    "current-time":Ember.computed("clock.time",function(){
        this.get("clock.time");
        return moment().format("h:mm:ss a z");
    }),
});
