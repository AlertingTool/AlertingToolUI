import Ember from 'ember';

export default Ember.Mixin.create({
    actions:{
        saveTask(id,name,description,recurringtime){
            this.store.findRecord('task', id).then((task)=>{
                task.setProperties({
                    name: name,
                    description: description,
                    recurringtime: recurringtime
                });
                task.save();
            });
        }
    }
});
