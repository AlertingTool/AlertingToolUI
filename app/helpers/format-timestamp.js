import Ember from 'ember';

export function formatTimestamp([value,time,pretty]) {
  if(pretty){
    return moment.unix(value).fromNow();
  }else{
    return moment.unix(value).format("MMMM Do YYYY, h:mm a");
  }
}

export default Ember.Helper.helper(formatTimestamp);
