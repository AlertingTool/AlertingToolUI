import Ember from 'ember';

export default Ember.Component.extend({
    classNames:["ui","modal"],
    opopen:Ember.observer("open",function(){
        if(this.get("open")){
            this.$().modal("show");
        }else{
            this.$().modal('hide');
        }
    }),
    didInsertElement(){
        if(this.get("open")){
            this.$().modal("show");
        }else{
            this.$().modal('hide');
        }
        this.$().modal({
            onHidden:()=>{
                if(!this.get("isDestroyed"))
                    this.set("open",false);
            }
        });
    },
    willDestroyElement(){
        this.$().modal('hide');
        this.$().modal('destroy');
    }
});
