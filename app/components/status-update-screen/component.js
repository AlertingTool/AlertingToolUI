import Ember from 'ember';

export default Ember.Component.extend({
    header:Ember.computed("model.name",function(){
        return `${this.get("model.name")} (${moment().format("MMMM Do YYYY")})`;
    }),
    actions:{
        setstatus(status){
            this.set("status",status);
        },
        save(status,description,jira,hours,minutes){
            this.sendAction("onClickSave",status,description,jira,hours,minutes);
        },
        clear(){
            this.setProperties({
                description:undefined,
                jira:undefined,
                hours:undefined,
                minutes:undefined
            });
        },
    }
});
