import Ember from 'ember';

export default Ember.Component.extend({
    actions:{
        add_task(name,description,hr,min){
            var time=(parseInt(hr||0)*60+parseInt(min||0))*60;
            this.sendAction("addTask",name,description,time);
        },
        save_task(name,description,hr,min){
            var time=(parseInt(hr||0)*60+parseInt(min||0))*60;
            this.sendAction("saveTask",name,description,time);
        }
    }
});
