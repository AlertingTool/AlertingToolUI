import Ember from 'ember';

export default Ember.Component.extend({
    clock: Ember.inject.service('clock-service'),
    timepassed:Ember.computed("task.time","clock.time",function(){
        this.get("clock.time");
        return moment.unix(this.get("task.time")).isBefore(moment());
    }),
    tomorrow:Ember.computed("task.time","clock.time",function(){
        this.get("clock.time");
        return moment.unix(this.get("task.time")).isSameOrAfter(moment().add(1, 'days').startOf("day"));
    }),
    updateHourMin:Ember.observer("task.recurringtime","task.name","task.description",function(){
        var time=this.get("task.recurringtime");
        this.setProperties({
            name:this.get("task.name"),
            description:this.get("task.description"),
            hour:Math.floor(time/3600),
            minute:Math.floor((time%3600)/60)
        });
    }).on("didInsertElement"),
    tagName:"",
    recurringtime:Ember.computed("task.recurringtime",function(){
        return moment.unix(moment().startOf("day").unix()+this.get("task.recurringtime")).format("h:mm A");
    }),
    edittable:false,
    setRecurringTime(hour,minute){
        console.log(hour,minute);
    },
    hour:0,
    minute:0,
    actions:{
        removeTask(id){
            this.sendAction("removeTask",id);
        },
        clickClock(){
            this.sendAction("onClickClock");
        },
        clickPencil(){
            this.toggleProperty("edittable");
        },
        saveTask(name,description,time){
            this.sendAction("onClickSave",this.get("task.id"),name,description,time);
        }
    }
});
