import Ember from 'ember';
import TaskMixin from 'alerttool/mixins/task';

export default Ember.Controller.extend(TaskMixin,{
    sortdef:["recurringtime:asc"],
    taskssorted:Ember.computed.sort('model',"sortdef"),
    tasks:Ember.computed('taskssorted.@each.name','search', function() {
        return this.get("taskssorted").filter((task)=>{
            return (task.get("name")||"").toLowerCase().indexOf((this.get("search")||"").toLowerCase())>=0;
        });
    }),
    actions:{
        addTask(name,description,recurringtime){
            this.get("store").createRecord("task",{
                name:name,
                description:description,
                time:moment().startOf("day").unix()+recurringtime,
                recurringtime:recurringtime
            }).save();
        },
        removeTask(id){
            this.get("store").findRecord('task', id).then(function(task) {
                task.set("active",false);
                task.save();
            });
        },
        download_logs(){
            this.get("store").findAll('log').then(function(logs){
                var text=logs.reduce(function(prev,cur){
                    var time=moment.unix(cur.get("time"));
                    return `${prev}${cur.get("task.name")},${cur.get("status")},${time.format("D-M-YYYY")},${time.format("H:m")},${cur.get("description")||""},${cur.get("by")},${cur.get("jira")||""},\n`;
                },"Name,Status,Date,Time,Description,Created by,JIRA/Freshdesk id,\n");
                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
                element.setAttribute('download', "log.csv");
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            })
        }
    }       
});
