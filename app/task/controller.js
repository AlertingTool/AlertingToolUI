import Ember from 'ember';
import TaskMixin from 'alerttool/mixins/task';

export default Ember.Controller.extend(TaskMixin,{
    clock: Ember.inject.service('clock-service'),
    popupopen:Ember.computed("timepassed","popupforced",{
        get(){
            return this.get("timepassed")||this.get("popupforced");
        },
        set(key, value) {
            return this.set("popupforced",value);
        }
    }),
    timepassed:Ember.computed("model.time","clock.time",{
        get(key){
            this.get("clock.time");
            return moment.unix(this.get("model.time")).isBefore(moment());
        }
    }),
    status:"complete",
    actions:{
        save(status,description,jira,hours,minutes){
            var task=this.get("model");
            if(status==="complete"){
                let next_trigger=moment().startOf('day').unix()+task.get("recurringtime");
                if(task.get("time")>=next_trigger){
                    next_trigger=moment().startOf('day').add(1, 'days').unix()+task.get("recurringtime");
                }
                task.set("time",next_trigger);
            }else{
                task.set("time",
                    moment().startOf('minute').unix()+(((parseInt(hours)||0)*60+(parseInt(minutes)||0))*60)
                );
            }
            this.get('store').createRecord('log', {
                description:description,
                jira:jira,
                status:status,
                time:moment().unix(),
                by:this.get("session.currentUser.email"),
                task: task
            }).save();
            task.save();
        },
        openpopup(){
            this.set("popupforced",true);
        }
    }
});
