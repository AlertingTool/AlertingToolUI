/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'alerttool',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    firebase: {
        apiKey: "AIzaSyCyqbyS8nxaau5Rz25ryL2vW3f95ummgz4",
        authDomain: "alertingtool.firebaseapp.com",
        databaseURL: "https://alertingtool.firebaseio.com",
        storageBucket: "alertingtool.appspot.com",
    },
    torii: {
      sessionServiceName: 'session'
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    ENV.firebase= {
        apiKey: "AIzaSyDpKNEHTkKlZHTjgku7daJvd1CF6PLR1QY",
        authDomain: "alertingtooldev.firebaseapp.com",
        databaseURL: "https://alertingtooldev.firebaseio.com",
        storageBucket: "alertingtooldev.appspot.com",
    };
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.firebase= {
        apiKey: "AIzaSyCyqbyS8nxaau5Rz25ryL2vW3f95ummgz4",
        authDomain: "alertingtool.firebaseapp.com",
        databaseURL: "https://alertingtool.firebaseio.com",
        storageBucket: "alertingtool.appspot.com",
    };
  }

  return ENV;
};
